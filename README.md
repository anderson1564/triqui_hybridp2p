###  README.md
## Triqui Hybrid P2P

#Intructions
	1. Compile the .java files in the folder src
    2. Run with java TriquiClientP2PRMI
    3. (Only the server pc)Select the port and start the server
    4. Connect to server
    5. Challenge another player

#Rules:
    The first player to put three 'X' or 'O' togthers win.

## Keywords:
	game, Triqui, hybrid, P2P
## Files:
	client:
           ClientGame.java    
           ClientP2PGUI.java  
           GameGUI.java    
           ITriquiClientP2PRMI.java  
           ServerGame.java              
           TriquiClientP2PRMI.java
           ClientP2PGUI.form  
           GameGUI.form       
           GameLogic.java  
           OpponentThread.java       
           TriquiClientP2PRMIImpl.java

    server:
           ITriquiServerRMI.java  
           TriquiServerRMIImpl.java  
           TriquiServerRMI.java

## Date:
	September 2013
## Author:
	Anderson L M
    Camilo Velasquez B