package server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class TriquiServerRMI {

    public static void main(String[] args) {
        String serverName = "TriquiServer";
        try {
            System.out.println("Please insert a port to start listening: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int port = Integer.parseInt(reader.readLine());
            TriquiServerRMIImpl server = new TriquiServerRMIImpl();
            Registry registry = LocateRegistry.createRegistry(port);
            registry.rebind(serverName, server);
            System.out.println(">>> Server Ready...");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
