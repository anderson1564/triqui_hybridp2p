package server;

import client.ITriquiClientP2PRMI;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author USUARIO
 */
public class TriquiServerRMIImpl extends UnicastRemoteObject implements ITriquiServerRMI {
    
    private HashMap<String, ITriquiClientP2PRMI> hashClientP2P;
    
    public TriquiServerRMIImpl () throws RemoteException{
        this.hashClientP2P = new HashMap();
    }

    @Override
    public boolean register(ITriquiClientP2PRMI clientP2P, String alias) throws RemoteException {
        if(!this.hashClientP2P.containsKey(alias)){
            this.hashClientP2P.put(alias, clientP2P);
            System.out.println("New client with alias " + alias + " registered");
            return true;
        }
        System.out.println("Client with alias " + alias + " already exists");
        return false;
    }

    @Override
    public void unRegister(String alias) throws RemoteException {
        this.hashClientP2P.remove(alias);
    }

    @Override
    public HashMap find() throws RemoteException {
        return this.hashClientP2P;
    }

    @Override
    public ITriquiClientP2PRMI localize(String alias) throws RemoteException {
        return this.hashClientP2P.get(alias);
    }
    
    
}
