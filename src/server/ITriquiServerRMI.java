package server;

import client.ITriquiClientP2PRMI;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author USUARIO
 */
public interface ITriquiServerRMI extends Remote{
    public boolean register(ITriquiClientP2PRMI clientP2P, String alias) 
            throws RemoteException;
    public void unRegister(String alias) throws RemoteException;
    public HashMap find() throws RemoteException;
    public ITriquiClientP2PRMI localize(String alias) throws RemoteException;
}
