package client;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author USUARIO
 */
public class ClientGame extends Thread{
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private GameGUI gameGUI;
    private boolean run = true;
    //Prueba
    
    public ClientGame(String host, int port, String alias){
        try {
            socket = new Socket(host,port);
            dis = new DataInputStream(socket.getInputStream());
	    dos = new DataOutputStream(socket.getOutputStream());
            gameGUI = new GameGUI(this, alias);
            gameGUI.setVisible(true);
        } catch (IOException ioe) {
            System.out.println("Creating socket: " + ioe);
        }
    }
    
    public void gamePlay(int pos){
        send("PLAY," + pos);
    }
    
    public void send(String msg){
         try {
            dos.writeUTF(msg);
        } catch (IOException ioe) {
            System.out.println("Sending message: " + ioe);
        }
    }
    
    private void decodeMsg(String msg) {
        String [] command = msg.split(",");
        switch (command[0]){ 
            case "PLAYER":
                gameGUI.resetText();
                String player = command[1];
                gameGUI.print("\nTURNO: " + player);
                break;  
            case "BOARD":
                String board = command[1];
                gameGUI.print(board);
                break;
            case "PLAY":
                switch (command[1]) {
                    case "no":
                        gameGUI.print(">>> Jugada invalida");
                        break;
                    case "wait":
                        gameGUI.disablePlay();
                        break;
                    default:
                        gameGUI.enablePlay();
                        break;
                }
                break;
            case "WINNER":
                gameGUI.resetText();
                if(!command[1].equals("N"))
                    gameGUI.print("\n" + command[1] + " woooon!!");
                else
                    gameGUI.print("\nNobody Won :(");
                break;
            case "QUIT":
                //TODO: Close communication
                break;
            default:
                break;
        }
    }

    @Override
    public void run() {
        while (run) {
            try {
                //recieve and decode
                String msg = dis.readUTF();
                decodeMsg(msg);
            } catch (IOException ioe) {
                System.out.println("Recieving message: " + ioe);
                run = false;
            }
        }
    }
}
