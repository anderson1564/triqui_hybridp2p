package client;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author USUARIO
 */
public class ServerGame {
    private TriquiClientP2PRMI client;
    private OpponentThread opponent;
    private boolean turn;
    private GameLogic gameLogic;
    private String winner;
    private int countPlay = 0;
    private GameGUI gameGUI;

    
    ServerGame(TriquiClientP2PRMI client, OpponentThread opponent, String alias){
        this.client = client;
        this.opponent = opponent;
        this.turn = true;
        this.gameLogic = new GameLogic();
        this.gameLogic.Start();
        this.gameGUI = new GameGUI(this, alias);
        this.gameGUI.setVisible(true);
    }

    public String getWinner() {
        return winner;
    }

    public int getCountPlay() {
        return countPlay;
    }
    
    public void startGame(){
        if(countPlay < 9){
            sendPlayers();
            sendBoard();
            sendPlay();
            ++countPlay;
        }else{
            winner = "N";
            sendWinner();
        }
    }
    
    public void sendPlayers(){
        String player = gameLogic.Player();
        opponent.send("PLAYER," + player);
        gameGUI.print("\nTURNO: " + player);
    }
    
    public void sendBoard(){
        String board = gameLogic.Board();
        opponent.send("BOARD," + board);
        gameGUI.print(board);
    }
    
    public void sendPlay(){
        if(turn){
           opponent.send("PLAY,yes");
           gameGUI.disablePlay();
        }else{
           opponent.send("PLAY,wait");
           gameGUI.enablePlay();
        }
    }
    
       public void gamePlay(int pos) {
        if(play(pos)){
            gameGUI.resetText();
            testWinner();
            if(winner.equals("N")){
                nextTurn();
                startGame();
            }else{
                sendWinner();
            }
        }else{
            gameGUI.print(">>> Jugada invalida");
        }
    }
    
    public boolean play(int pos){
        return gameLogic.Play(pos);
    }
    
    public void testWinner(){
        winner = gameLogic.TestWinner();
    }
    
    public void sendWinner(){
        opponent.send("WINNER," + winner);
        gameGUI.showWinner(winner);
        sendBoard();
    }
    
    public void nextTurn(){
        turn = !turn;
    }

    void resetText() {
        gameGUI.resetText();
    }
}
