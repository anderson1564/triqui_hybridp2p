package client;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author USUARIO
 */
public interface ITriquiClientP2PRMI extends Remote{
    public String getHost() throws RemoteException;
    public int getPort() throws RemoteException;
}
