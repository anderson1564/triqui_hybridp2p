/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import server.ITriquiServerRMI;

/**
 *
 * @author USUARIO
 */
public class TriquiClientP2PRMI extends Thread{
    private ClientGame thread;
    private ArrayList<OpponentThread> opponents;
    private int port;
    private boolean wait;
    private ServerGame game;
    private ITriquiServerRMI serverRMI;
    private String alias;
    
    public TriquiClientP2PRMI(){
        opponents = new ArrayList();
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean register(String alias, String host, int port) throws 
            NotBoundException, RemoteException, UnknownHostException{
        this.alias = alias;
        String serverName = "TriquiServer";
        ITriquiClientP2PRMI client = new TriquiClientP2PRMIImpl(
                InetAddress.getLocalHost().getHostAddress(), this.port);
        Registry registry = LocateRegistry.getRegistry(host, port);
        serverRMI = (ITriquiServerRMI)registry.lookup(serverName);
        return serverRMI.register(client, alias);
    }
    
    public HashMap listClients(){
        HashMap cClients = null;
        try {
            cClients = serverRMI.find();
        } catch (RemoteException ex) {
            System.out.println("Error on listClients(): " + ex.getMessage());
        }
        return cClients;
    }
    
    public void challenge(String alias){
        try {
            ITriquiClientP2PRMI client = serverRMI.localize(alias);
            if(client != null){
                thread = new ClientGame(client.getHost(), client.getPort(), this.alias);
                thread.start();
            }else{
                JOptionPane.showMessageDialog(null, alias + " not found", 
                        "Warning", JOptionPane.WARNING_MESSAGE);
            }
        } catch (RemoteException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }
    
    @Override
    public void run() {
        wait = true;
        ServerSocket server;
        try {
            server = new ServerSocket(port);
            //wait and accept new connections
            while (wait) {
                Socket newClient = server.accept();
                System.out.println("New opponent!");
                OpponentThread opponent = new OpponentThread(this, newClient);
                opponents.add(opponent);
                opponent.start();
                game = new ServerGame(this, opponent, this.alias);
                opponent.setGame(game);
                game.startGame();
            }
            //TODO: Code for closing sockets!
        } catch (IOException ioe) {
            System.out.println("Creating server socket: " + ioe);
            wait = false;
        }
    }
    
    public static void main(String arg[]){
        TriquiClientP2PRMI clientP2P = new TriquiClientP2PRMI();
        ClientP2PGUI clientGUI = new ClientP2PGUI(clientP2P);
        clientGUI.setVisible(true);
    }
}
