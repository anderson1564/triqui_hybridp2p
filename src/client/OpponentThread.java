package client;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author USUARIO
 */
public class OpponentThread extends Thread{
    private TriquiClientP2PRMI client;
    private Socket socket;//For closing conn
    private DataInputStream dis;
    private DataOutputStream dos;
    private ServerGame game;
    private boolean run = true;
    
    public OpponentThread(TriquiClientP2PRMI client, Socket socket) {
        this.client = client;
        this.socket = socket;
        try {
            //obtain input and output streams from the client
            dis = new DataInputStream(socket.getInputStream());
	    dos = new DataOutputStream(socket.getOutputStream());
        } catch (IOException ioe) {
            System.out.println("Obtaining data streams: " + ioe);
        }
    }

    public void setGame(ServerGame game) {
        this.game = game;
    }
    
    public void send(String msg){
         try {
            dos.writeUTF(msg);
        } catch (IOException ioe) {
            System.out.println("Sending message: " + ioe);
        }
    }
    
    private void decodeMsg(String msg) {
        String [] command = msg.split(",");
        switch (command[0]){ 
            case "PLAY":
                int pos = Integer.parseInt(command[1]);
                if(game.play(pos)){
                    game.resetText();
                    game.testWinner();
                    if(game.getWinner().equals("N")){
                        game.nextTurn();
                        game.startGame();
                    }else{
                        game.sendWinner();
                    }
                }else{
                    send("PLAY,no");
                }
                break;
            case "QUIT":
                //TODO: Close communication
                break;
            default:
                break;
        }
    }
    
    @Override
    public void run() {
         while (run) {
            try {
                //recieve and decode the JSON objects sent by the clients
                String msg = dis.readUTF();
                decodeMsg(msg);
            } catch (IOException ioe) {
                System.out.println("Error recieving message: " + ioe);
                run = false;
            }
        }
        //TODO: Code for deallocating this client from the server!
    }
    
}
