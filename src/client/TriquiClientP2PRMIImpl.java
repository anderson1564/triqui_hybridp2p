/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author USUARIO
 */
public class TriquiClientP2PRMIImpl extends UnicastRemoteObject implements ITriquiClientP2PRMI{
    private String host;
    private int port;

    public TriquiClientP2PRMIImpl(String host, int port) throws RemoteException {
        this.host = host;
        this.port = port;
    }

    @Override
    public String getHost() throws RemoteException {
        return host;
    }

    @Override
    public int getPort() throws RemoteException {
        return port;
    }
    
}
